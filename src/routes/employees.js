const express = require('express');
const mongoose = require('mongoose');
const co = require('co');
const Employees = require('../models/employees'); // Model
const Companies = require('../models/companies'); // Model

// Initiating router
const router = express.Router(); // Used to assign into a Node server route

/** Find all employees */
router.get('/', (req, res) => {
	Employees.find({}, (err, employees) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!employees) res.status(401).json({ error: "Unauthorized action!" });
		// Everything OK
		res.json({ success: true, employees }); // Return collection saved including the MongoDB _id
	});
})

/** Find employee by identifier (id) */
router.get('/:id', (req, res) => {
	let _id = req.params.id || null;

	// Basic identifier validation
	if (!_id)
		res.status(400).json({ success: false, error: "Invalid identifier has been sent!" });

	// Converting ID to OID through mongoose
	_id = mongoose.Types.ObjectId(_id);

	// Querying by document '$oid'
	Employees.find({ _id }, (err, employee) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!employee) res.status(401).json({ error: "Unauthorized action!" });
		// Everything OK
		res.json({ success: true, employee });
	});
})

/** Find employee by identifier (id) */
router.get('/by-name/:name', (req, res) => {
	let name = req.params.name || null;

	// Basic identifier validation
	if (!name) res.status(400).json({ success: false, error: "Invalid identifier has been sent!" });

	// Querying by document 'name'
	// If you like to limit for the 5 firts use: Employees.find({name: { $regex: '.*' + name + '.*' } }).limit(5);
	Employees.find({name: { $regex: '.*' + name + '.*' } }, (err, employees) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!employees) res.status(401).json({ error: "Unauthorized action!" });
		// Everything OK
		res.json({ success: true, employees });
	});
})


/** New employee with generator and coroutine */
router.post('/gen', (req, res) => {
	co(function* () {
		const { idCompany, name, departament } = req.body;
		const _id = mongoose.Types.ObjectId();
		const company = yield Companies.findOne({ _id: idCompany }).exec();
		const employeeCreated = yield Employees.create({ _id, idCompany, name, departament });
		const { employees } = company;
		employees.push(_id);
		yield Companies.findByIdAndUpdate({ _id: idCompany }, { $set: { employees } }, { new: true }).exec();
		res.status(201).json({ success: true, employeeCreated });
	}).catch((err) => res.status(400).json({ error: "Invalid request, something went wrong!", err }));
});

/** New employee with async/await */
router.post('/', async (req, res) => {
	try {
        const { idCompany, name, departament } = req.body;
        const _id = mongoose.Types.ObjectId();
        const company = await Companies.findOne({ _id: idCompany }).exec();
        const employeeCreated = await Employees.create({ _id, idCompany, name, departament });
        const { name: companyName, address, email, tel, employees } = company;
        employees.push(_id);
        await Companies.findByIdAndUpdate(
                    { _id: idCompany },
                    { $set: { _id: idCompany, name: companyName, address, email, tel, employees } },
                    { new: true }).exec();
        res.status(201).json({ success: true, employeeCreated });
    } catch(err) {
        res.status(400).json({ error: "Invalid request, something went wrong!", err });
    }
});

/** Update employee */
router.put('/', (req, res) => {
	let { _id, idCompany, name, departament } = req.body;

	// Find the employee by it's ID and update it
	Employees.findByIdAndUpdate(
		_id,
		{ $set: { idCompany, name, departament }}, // spotlight
		{ new: true },
		(err, employee) => {
			// Something wrong happens
			if (err) res.status(400).json({ success: false, error: "Can't update employee!" });
			// Everything OK
			res.json({ success: true, employee });
		}
	);
});

/** Delete employee */
router.delete('/:id', (req, res) => {
	const _id = req.params.id || null;
	let employeeSelect;
	let companySelect;

	// Remove employee by it's _ID
	if (_id) {
		const promise1 = new Promise((resolve, reject) => {
			// Querying by document '$oid'
			Employees.find({ _id }, (err, employee) => {
				// Error returned
				if (err) reject({ error: "Invalid request, something went wrong!" });
				// Invalid data received
				if (!employee) reject({ error: "Unauthorized action!" });
				// Everything OK
				employeeSelect = employee[0];
				resolve(employee);
			});
		})
		.then(() => {
			const promise2 = new Promise((resolve, reject) => {
				Employees.deleteOne({ _id }, err => {
					// Something wrong happens
					if (err) throw new Error({ success: false, error: "Can't remove employee!" });
					// OK response
					resolve();
				});
			});
			return promise2;
		})
		.then(() => {
			const promise3 = new Promise((resolve, reject) => {
				let { idCompany } = employeeSelect;

				// Querying by document '$oid'
				Companies.find({ _id: idCompany }, (err, company) => {
					// Error returned
					if (err) reject({ error: "Invalid request, something went wrong!" });
					// Invalid data received
					if (!company) reject({ error: "Unauthorized action!" });
					// Everything OK
					companySelect = company[0];
					resolve();
				});
			});
			return promise3;
		})
		.then(() => {
			const promise4 = new Promise((resolve, reject) => {
				let { _id : idCompany, name, address, email, tel, employees } = companySelect;
				employees = employees.filter( idEmployee => idEmployee != _id );

				// Find the company by it's ID and update it
				Companies.findByIdAndUpdate(
					{ _id: idCompany },
					{ $set: { _id: idCompany, name, address, email, tel, employees  } }, // spotlight
					{ new: true },
					(err, company) => {
						// Something wrong happens
						if (err) res.status(400).json({ success: false, error: "Can't update company!" });
						// Everything OK
						resolve();
					}
				);
			});
			return promise4;
		});

		promise1
			.then(() => res.json({ success: true }))
			.catch(err => console.log('error in promise chain', err));

	} else {
		res.status(400).json({ error: "Source required to perform the search!" });
	}
});

module.exports = router; // Exporting as a default router
