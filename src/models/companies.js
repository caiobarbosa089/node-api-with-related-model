const mongoose = require('mongoose');

const companiesSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	name: String,
	address: {
		state: String,
		city: String,
		neighborhood: String,
		street: String,
		number: String
	},
	email: String,
	tel: String,
	employees: [String]
}, {
	versionKey: false // Unable auto-version after persist database
});

const Companies = mongoose.model('companies', companiesSchema);

module.exports = Companies;
